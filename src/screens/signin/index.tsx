import * as React from 'react'
import { observer } from 'mobx-react'
import { observable } from 'mobx'

interface TypeStore {
  count: number
}

const Minus = observer(({ store }: { store: TypeStore}) => {
  function minus () {
    store.count--
  }
  return <button onClick={minus}>-</button>
})

@observer
export class SignIn extends React.Component {
  @observable public store: TypeStore = {
    count: 0
  }
  public plus () {
    this.store.count++
  }
  public render () {
    return (
      <div>
        <div>
          <button onClick={this.plus.bind(this)}>+</button>
          <Minus store={this.store} />
        </div>
        {`Count : ${this.store.count}`}
      </div>
    )
  }
}
