import * as React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

// Import Component
import { SignIn } from './signin'

import '../css/theme.css'

export class Router extends React.Component {
  public render () {
    return (
      <BrowserRouter>
        <div>
          <Switch>
            <Route exact={true} path='/' render={() => <SignIn />} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}
