import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { Router } from './screens/Router'

ReactDOM.render(<Router />, document.getElementById('root') as HTMLElement)
